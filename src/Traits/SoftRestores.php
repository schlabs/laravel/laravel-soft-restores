<?php
namespace SchLabs\LaravelSoftRestores\Traits;

/**
 * For Models
 * Restores or create
 */
trait SoftRestores
{
    /**
     * Create or restore a model
     * @param array $attributes
     * @param array $values
     * @return bool|null
     */
    public static function restoreOrCreate(array $attributes, array $values = []): ?bool
    {
        $instance = self::withTrashed()->firstOrNew($attributes);

        //Check if exists and restore it
        if($instance->exists){
            return $instance->restore();
        }

        //Otherwise, create it
        return $instance->fill($values)->save();
    }
}
