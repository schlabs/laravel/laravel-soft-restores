
# laravel-soft-restores
  

## Description

Soft restore for your soft deleted models
  

## Depencencies

* none
  

## Installation
  

**In your composer.json**

Add the following code to the "repositories" key:

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://gitlab.com/schlabs/laravel/laravel-soft-restores"
	}
]
```

Add this line to your require dependecies:

```json
"schlabs/laravel-soft-restores": "1.0.*"
```
 
**In your project root run:**
```sh
composer update
```

## Functions

```
/**
 * Create or restore a model
 * @param array $attributes Attributes to find model
 * @param array $values Values to include in new model
 * @return bool|null
 */
createOrRestore($attributes, $values): bool
```

## Usage

1. Include the "Illuminate\Database\Eloquent\SoftDeletes" trait in your model
2. Include the "SchLabs\LaravelSoftRestores\Traits\SoftRestores" trait in your model.
3. Use the function "createOrRestore" to create or restore (if exists) a model

## Example

```
User::createOrRestore([
    'identifier' => '1234'
], [
    'name' => 'John Doe'
])
```


